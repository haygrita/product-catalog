# Product Cart 

This project is a demonstration of features of React in terms of Component update and rendering. It covers an example of a product list  from which needed product and its quantity can be added to cart by clicking on 'Add to Cart' which can either be purchased in future or be removed from cart on click of 'Remove from cart' button .  '+' is to increase product count. '-' descrease product count. 


## PreRequisite
 
 Node - v6+
 Mysql

 ## FrameWork used
 React + Node + Express + Mysql

 ## Dependency added 

sequelize - promise based ORM for nodeJs. It is used to interact with database. 
mysql2 - MySql client for Node.
axios - promise based http client for request and response sent between client and server
react , react-dom  
 Mysql

## FrameWork used ##

 React + Node + Express + Mysql

## Dependency added ##

sequelize - promise based ORM for nodeJs. It is used to interact with database. 

mysql2 - MySql client for Node.

axios - promise based http client for request and response sent between client and server

react, react-dom 


## Step to run ##

 To run server -> node app.js
 
 To run client -> npm start





