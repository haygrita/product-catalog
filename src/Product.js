import React,{Component} from 'react';

class Product extends Component{
	render(){
   let prod=this.props.item;
   let button;
   if(prod.productStatus==="RMV"){
             button=<button onClick={()=>{this.props.removefromCart(prod)}}>Remove from Cart</button>;
                }
         else if(prod.productStatus==="ADD"){
             button=<button disabled="true">Added</button>;
         }
             else{
              button=<button onClick={()=>{this.props.addtoCart(prod)}}>Add to cart</button> ;
               }
	return(
        <div className='product'>
             <p>{prod.productName}</p>
             <p>{prod.description}</p>
             <p>Price: {prod.price}</p>
             <div>
             {
                prod.productStatus==="RMV"?
                <div> 
             <p>Quantity:</p>
             <button onClick={()=>this.props.increaseProdCount(prod)}>+</button>
             {prod.quantity} 
             <button onClick={()=>this.props.decreaseProdCount(prod)}>-</button>
             {prod.amount>0?<p>Amount: {prod.amount}</p>:null}           
            </div>   

             :null}
             </div>
   
             {button}
        </div>      
     
	);
}

}


export default Product;