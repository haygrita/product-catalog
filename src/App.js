import React, { Component } from 'react';
import Products from './Products';
import './App.css';
import axios from 'axios';
import {Link,Switch,Route} from 'react-router-dom'
import ProductAdd from './ProductAdd';


class App extends Component {
  constructor(){
    super();
    this.state={
      dataset:[],
      orderedProducts:[]
    }
    this.increaseProdCount=this.increaseProdCount.bind(this);
    this.decreaseProdCount=this.decreaseProdCount.bind(this);
    this.addtoCart=this.addtoCart.bind(this);
    this.removefromCart=this.removefromCart.bind(this);

  }

     componentWillMount(){
      axios.get('http://localhost:8888/productList').then((res)=>{
            var dataset=res.data;
           dataset.map(data=>{
                 data.quantity=1;
                 data.amount=data.price*data.quantity;
            });
       this.setState({dataset: dataset})

          });

       axios.get('http://localhost:8888/orderedProducts').then((res)=>{
            var dataset=res.data;
            dataset.map(data=>{
               data.productName=data.productDetail.productName,
               data.description=data.productDetail.description,
               data.price=data.productDetail.price,
               data.productStatus="RMV"
            });
       this.setState({orderedProducts: dataset})

          });
}

    
      componentWillReceiveProps(nextProps){
         console.log(nextProps);
      }

  
     
   increaseProdCount(product){
     this.state.orderedProducts.map(data=>{
        if(data.productID===product.productID){
          data.quantity+=1;
          data.amount=data.price*data.quantity;
          axios.put('http://localhost:8888/updateStock',data);
        }
        
     })
     this.setState({
        orderedProducts:this.state.orderedProducts
     })
  }

  decreaseProdCount(product){
     this.state.orderedProducts.map(data=>{
        if(data.productID==product.productID){
          data.quantity>0?data.quantity-=1:data.quantity;
          data.amount>0?data.amount=data.price*data.quantity:null;
          axios.put('http://localhost:8888/updateStock',data);

        }
     })
     this.setState({
        orderedProducts:this.state.orderedProducts
     })
  }

  addtoCart(product){
    let x=this.state.orderedProducts.filter(order=>order.productID==product.productID);
    if(x.length!==0){alert("product already ordered");}
    else{
      var data={
      productID:product.productID,
      quantity:product.quantity,
      amount:product.amount
    }
    
     axios.post('http://localhost:8888/productOrder',data).then((res)=>{
       alert("added to cart" , JSON.stringify(data) );
       this.state.dataset.map(product=>{
         if(product.productID===data.productID){
               product.productStatus="ADD"
         }
       })
       this.setState({
           dataset:this.state.dataset
       });

   })
  }
  }

 removefromCart(product){
      axios.delete('http://localhost:8888/deleteProduct/'+product.productID).then(res=>{
        console.log('deleted frm cart'+res);
      })
 }  

  render() {
     let message;
     this.state.dataset.map(data=>{
        this.state.orderedProducts.map(order=>{
             if(data.productID===order.productID){
                  data.productStatus="ADD"
             }
        })
     })

     if(this.state.orderedProducts.length===0){
            message="No items added to cart yet"
     }
    return (
      <div className='App'>
         <Link to='/addProduct'>Add Product</Link>
              <div>
                 <h5>Product List</h5>
         <div className='flex-container'>

         <Products
          products={this.state.dataset.filter(data=> data.productStatus!=="RMV")} 
          addtoCart={this.addtoCart} />
         </div>
       <h5> Added to cart </h5>
         
                  {message}
             <div className='flex-container'>

            <Products products={this.state.orderedProducts} increaseProdCount={this.increaseProdCount} decreaseProdCount={this.decreaseProdCount}  removefromCart={this.removefromCart}></Products>
            </div>
       </div>
      </div>
    );
  }
}

export default App;
