import React,{Component} from 'react';
import axios from 'axios';


class ProductAdd extends Component{
     constructor(){
        super();
        this.handleClick=this.handleClick.bind(this);
     }

	handleClick(event){
		event.preventDefault();
		let data={
			productName:this.refs.productName.value,
			description:this.refs.description.value,
			price:this.refs.price.value
		}
        axios.post('http://localhost:8888/addProduct',data).then((res)=>{
        	alert("product added"+JSON.stringify(res.data));
        })
	}
	render(){
		return(
			<div className="App">
			<form>
              Product Name:<input type="text" ref="productName"/>
              Description :<input type="text" ref="description"/>
              Price :<input type="text" ref="price"/>
              <button onClick={this.handleClick}>Add product </button>


			</form>
			</div>
			)
	}
}

export default ProductAdd;