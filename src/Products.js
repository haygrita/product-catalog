import React,{Component} from 'react';
import Product from './Product';
import {Link} from 'react-router-dom';
class Products extends Component{
render(){
  return this.props.products.map(product=>{
	return (
		 <div>
			<Product item={product} key={product.id} increaseProdCount={this.props.increaseProdCount} decreaseProdCount={this.props.decreaseProdCount}
			addtoCart={this.props.addtoCart} removefromCart={this.props.removefromCart}/>

			</div>
		);
	})
}
}


export default Products;