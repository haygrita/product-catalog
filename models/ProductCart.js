var sequelize=require('./model');
var Sequelize = require('sequelize');
var productOrders=sequelize.define('productCart',{
         productID:{
         	type:Sequelize.INTEGER,
         	field:'product_id'
         },
         quantity:{
         	type:Sequelize.INTEGER,
         	field:'quantity'
         }, amount:{
         	type:Sequelize.DOUBLE,
         	field:'amount'
         }
        },
        {
	tableName:'product_cart_tbl',
	  freezeTableName: true,
	  timestamps: false
	});

module.exports=productOrders;