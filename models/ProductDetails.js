var sequelize=require('./model');
var Sequelize = require('sequelize');

var productDetails=sequelize.define('productDetails',{
         productID:{
         	type:Sequelize.INTEGER,
         	primaryKey:true,
         	autoIncrement:true,
         	field:'product_id'
         }, productName:{
         	type:Sequelize.STRING,
         	field:'product_name'
         }, description:{
         	type:Sequelize.STRING,
         	field:'description'
         }, price:{
         	type:Sequelize.DOUBLE,
         	field:'price'
         }
     },
         {
	tableName:'product_details_tbl',
	  freezeTableName: true,
	  timestamps: false

})


module.exports=productDetails;