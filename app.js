var express=require('express');
var app=express();
var bodyParser=require('body-parser');
var productDetails=require('./models/ProductDetails');
var productOrders=require('./models/ProductCart');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

productOrders.belongsTo(productDetails,{foreignKey:'product_id'});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE,OPTIONS');

  next();
});

app.get('/productList',function(req,res){
     productDetails.findAll().then(function(data){
           res.send(data);
     });
});


app.post('/productOrder',function(req,res){
    var order=req.body;
	productOrders.build(order).save().then(function(response){
		console.log(response);
		res.send(response.dataValues);
	})

});


app.post('/addProduct',function(req,res){
    var product=req.body;
    console.log(product);
    productDetails.build(product).save().then(function(response){
    console.log(response);
    res.send(response.dataValues);
  })

});

app.get('/orderedProducts',function(req,res){
         productOrders.findAll({
         	include:{
         		model:productDetails,
         		attribute:['productName','description','price'],
         		required:true
         	}
         }).then(function(data){
         	res.send(data);
         })
});


app.put('/updateStock',function(req,res){
    var data=req.body;
    productOrders.find({where:{
        productID:data.productID
    }}).then(function(item){
      item.updateAttributes({
          quantity:data.quantity,
          amount:data.amount
      })
    }).then(function(result){
      res.send(result);
    })

})

app.delete('/deleteProduct/:id',function(req,res){
	var id=req.params.id;
	productOrders.destroy({
		where:{
			productID:id
		}
	}).then(function(response){
		res.status(200);
	})
})

app.listen(8888);